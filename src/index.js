import React from 'react';
import ReactDOM from 'react-dom';
import App from './js/app';
import '../assets/styles/global.scss';

ReactDOM.render(<App />, document.getElementById("root"));