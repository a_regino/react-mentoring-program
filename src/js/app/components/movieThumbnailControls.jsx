import React, {Component} from 'react';
import  RadioButtonsSet from './radioButtonSet';

export default class MovieThumbnailControls extends Component {
    render() {
        return(
            <section className= 'movie-controls'>
                <span className='movie-controls-title'>{this.props.searchEnabled ? '7 films found' : 'Films by Drama genre'}</span>
                <RadioButtonsSet setTitle= 'SORT BY' leftLabel= 'RELEASE DATE' rightLabel= 'RATING'/>
            </section>
        );
    }
}