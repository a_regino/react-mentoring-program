import React, {Component} from 'react';
import RadioButtonsSet from './radioButtonSet';

export default class Search extends Component {
    render() {
        return(
            <section>
                <p className='search-title'>FIND YOUR MOVIE</p>
                <form className='search'>
                    <input type="text" placeholder='Search' className='search-input'></input>
                    <button className='search-btn'>SEARCH</button>
                </form>
                <RadioButtonsSet setTitle= 'SEARCH BY' leftLabel= 'TITLE' rightLabel= 'GENRE'/>
            </section>
        );
    }
}
