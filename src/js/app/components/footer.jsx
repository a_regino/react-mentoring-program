import React, {Component} from 'react';
import Logo from './logo';

export default class Footer extends Component {
    render() {
        return(
            <footer className= 'footer'>
                <div className='netflixroulette-logo'>
                    <Logo/>
                </div>
            </footer>
        );
    }
}