import React, {Component} from 'react';
import Search from './search';
import MovieDetail from './movieDetail';
import Logo from './logo';
import searchIcon from "../../../../assets/images/search.png";

export default class Header extends Component {
    render() {
        const genres= [
            "Action",
            "Adventure",
            "Science Fiction"
        ];

        const showMovieDetails = false;
        return(
            <header className= 'header'>
                <div className='logo-header'>
                    <Logo/>
                </div>
                {!showMovieDetails ?
                <div className='search-parent'>
                    <div className='search-align'>  
                        <Search/>
                    </div>    
                </div> :
                <div>
                    <div className='search-icon'>
                        <img src={searchIcon}></img>
                    </div>
                    <MovieDetail
                        posterPath='https://image.tmdb.org/t/p/w500/ldoY4fTZkGISMidNw60GHoNdgP8.jpg'
                        title= "Guardians of the Galaxy Vol. 3"
                        genres= {genres}
                        runtime= {145}
                        voteAverage={4.5}
                        releaseDate= "2020-05-01"
                        overview= "We all have a superhero inside us, it just takes a bit of magic to bring it out. In Billy Batson's case, by shouting out one word--SHAZAM!--this streetwise 14-year-old foster kid can turn into the adult superhero Shazam, courtesy of an ancient wizard.Still a kid at heart--inside a ripped, godlike body--Shazam revels in this adult version of himself by doing what any teen would do with superpowers: have fun with them! Can he fly? Does he have X-ray vision? Can he shoot lightning out of his hands? Can he skip his social studies test? Shazam sets out to test the limits of his abilities with the joyful recklessness of a child. But he'll need to master these powers quickly in order to fight the deadly forces of evil controlled by Doctor Thaddeus Sivana."/>
                </div>}
            </header>
        );
    }
}
