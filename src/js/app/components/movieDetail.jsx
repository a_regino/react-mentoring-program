import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class MovieDetail extends Component {
    render() {
        const {posterPath, title, voteAverage, genres, releaseDate, runtime, overview} = this.props;

        return(
            <section className='movie-detail'>
                <img src={posterPath} alt={`Poster of ${title}.`} className="movie-detail-img"/>
                <div className="movie-detail-desc">
                    <div className='movie-detail-desc-line'>
                        <span className='movie-detail-desc-title'>{title}</span>
                        <div className='movie-detail-desc-rating-border'>
                            <span className='movie-detail-desc-rating'>{voteAverage}</span>
                        </div>
                    </div>
                    <span className='movie-preview-genre'>{genres.join(' & ')}</span>
                    <div className='movie-detail-desc-line'>
                        <div>
                            <span className='movie-detail-desc-year-and-runtime'>{parseInt(releaseDate)}</span>
                            <span className='movie-detail-desc-second-plan'> year</span>
                        </div>
                        <div className='movie-detail-desc-runtime-block'>
                            <span className='movie-detail-desc-year-and-runtime'>{runtime}</span>
                            <span className='movie-detail-desc-second-plan'> min</span>
                        </div>
                    </div>
                    <article className='movie-detail-desc-overview'>{overview}</article>
                </div>
            </section>
        );
    }
}

MovieDetail.propTypes = {
    posterPath: PropTypes.string,
    voteAverage: PropTypes.number,
    title: PropTypes.string,
    genres: PropTypes.array,
    releaseDate: PropTypes.string,
    runtime: PropTypes.number,
    overview: PropTypes.string
  };