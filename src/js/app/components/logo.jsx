import React from 'react';

const Logo = () => {
    return(
        <ul className='netflixroulette-logo'>
            <li className='fat'>netflix</li>
            <li className='light'>roulette</li>
        </ul>
    );
}

export default Logo;