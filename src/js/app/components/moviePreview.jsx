import React, {Component} from 'react';
import PropTypes from 'prop-types';

function MoviePreview (props) {
    const {posterPath, title, releaseDate, genres} = props;

    return(
        <div className='movie-preview'>
            <figure className='movie-preview-figure'>
                <img src={posterPath} className="movie-preview-img" />
                <figcaption>
                    <div className='movie-preview-first-line'>
                        <span className='movie-preview-title'>{title}</span>
                        <div className='movie-preview-year-rect'>
                            <span className='movie-preview-year'>{parseInt(releaseDate)}</span>
                        </div>
                    </div>
                    <span className='movie-preview-genre'>{genres.join(' & ')}</span>
                </figcaption>
            </figure>
        </div>
    );
}

export default MoviePreview;

MoviePreview.propTypes = {
    posterPath: PropTypes.string,
    title: PropTypes.string,
    genres: PropTypes.array,
    releaseDate: PropTypes.string,
  };