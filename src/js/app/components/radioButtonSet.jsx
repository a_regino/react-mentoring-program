import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class RadioButtonsSet extends Component {

    render() {
        const {setTitle, leftLabel, rightLabel} = this.props;

        return(
            <section className='radio-toolbar'>
                <span className='title'>{setTitle}</span> 
                <input type="radio" id={leftLabel} checked="checked" name={setTitle} />
                <label for={leftLabel} className='label-left'>{leftLabel}</label>
                <input type="radio" id={rightLabel} name={setTitle}/>
                <label for={rightLabel} className='label-right'>{rightLabel}</label>
            </section>
        );
    }
}

RadioButtonsSet.propTypes = {
    setTitle: PropTypes.string,
    leftLabel: PropTypes.string,
    rightLabel: PropTypes.string
  };