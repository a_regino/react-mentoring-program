import React, {Component} from 'react';
import MoviePreview from './moviePreview';

export default class MoviesThumbnail extends Component {
    render() {
        const movie = {
            posterPath: "https://image.tmdb.org/t/p/w500/ldoY4fTZkGISMidNw60GHoNdgP8.jpg",
            title: "Guardians of the Galaxy Vol. 3",
            genres: [
                "Action",
                "Adventure",
                "Science Fiction"
            ],
            releaseDate: "2020-05-01"
        };

        const movies = [movie, movie, movie, movie, movie];

        return(
             <div className='movies-thumbnail'>
                {
                    movies ? movies.map(m => 
                        <MoviePreview posterPath={m.posterPath}
                        title= {m.title}
                        genres= {m.genres}
                        releaseDate= {m.releaseDate}
                        /> 
                    ) :
                    ( <div className='movies-thumbnail-no-films-found'>
                        No films found
                     </div> 
                    )
                }
            </div>
        );
    }
}