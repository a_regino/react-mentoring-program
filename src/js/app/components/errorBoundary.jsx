import React, { Component } from "react";

import oops from "../../../../assets/images/oops.jpg";

export default class ErrorBoundary extends Component {
    constructor(props) {
      super(props);
      this.state = { error: null };
    }
  
    componentDidCatch(error, errorInfo) {
      this.setState({ error });
    }
    
    render() {
      return (
        this.state.error ?
          <div className='oops-img'>
            <div>
              <img src={oops} alt='Error occured picture.'/>
              <div className='oops-message'>
                <p >We're sorry - something's gone wrong.</p>
              </div>
            </div>
          </div>
          : this.props.children
      );
    }
  }