import React, {Component} from 'react';
import Header from './app/components/header';
import Footer from './app/components/footer';
import MovieThumbnailControls from './app/components/movieThumbnailControls';
import MoviesThumbnail from './app/components/moviesThumbnail';
import ErrorBoundary from './app/components/errorBoundary';

export default class App extends Component {
    render() {
        return(
            <>
                <ErrorBoundary>
                    <Header/>
                    <MovieThumbnailControls/>
                    <MoviesThumbnail/>
                    <Footer/>
                </ErrorBoundary>
            </>
        );
    }
}
